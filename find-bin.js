var fs = require('fs');
var pad = require( 'utils-pad-string' );
console.log("Bin file dump analysis tool - v0.2 - Hello Sander, I'm having so much fun :)");

console.log("***************************************************");
console.log("***  Parsing BIN JSON file                      ***");
console.log("***************************************************");
var ranges = [];
try {
    ranges = JSON.parse(fs.readFileSync(process.argv[2] || 'bindata.json', 'utf8'));
} catch (error) {
    console.log('No consolidated bin ranges file found.');
}
console.log("Parsed bin file dump, amount of ranges : " + ranges.length);
var offendingRange = process.argv[3] || 54393;
console.log("Looking for any range that include " + offendingRange);

var extractNumericSubRange = function (range, subRangeLength) {
    return parseInt(range.substring(0, subRangeLength));
};

var isInRange = function(subRange, range) {
    var subRangeStart = extractNumericSubRange(range.RangeStart || range.rangeStart, subRange.toString().length);
    var subRangeEnd = extractNumericSubRange(range.RangeEnd || range.rangeEnd, subRange.toString().length);
    return subRange >= subRangeStart && subRange <= subRangeEnd;
};

ranges.forEach(function(range) {
    try {
        if (isInRange(offendingRange, range)) {
            console.log(range);
        }
    } catch (e) {
        console.log('Error processing range ' + JSON.stringify(range));
        console.log(e);
    }
});

var parse = require('csv-parse');
var sourceFiles = [];

var sourceFilesPath = './source-files/'

console.log("***************************************************");
console.log("***  Parsing BIN CSV files                      ***");
console.log("***************************************************");

fs.readdir(sourceFilesPath, function (err, items) {
    if (err) {
        console.log('Error reading source files.');
        console.log(err);
        process.exit(-1);
    }
    sourceFiles = items;
    parseNextSource();
});

var parseNextSource = function() {
    if (sourceFiles.length > 0) {
        var file = sourceFiles.pop();
        var input = fs.createReadStream(sourceFilesPath + file);
        input.pipe(generateParser(file));
    } else {
        process.exit(0);
    }
};

var generateParser = function(file) {
    var foundRanges = [];
    
    var parser = parse({
        columns: true
    });
    
    parser.on('readable', function(){
        while(record = parser.read()){
            var range = {
                RangeStart: record.RangeStart,
                RangeEnd: record.RangeEnd,
                Scheme: record.Scheme,
                AcceptanceNetworkName: record.AcceptanceNetworkName,
                Country: record.IssuerCountry,
                Currency: record.Currency
            };
            if (isInRange(offendingRange, range)) {
                foundRanges.push(range);
            }
        }
    });
    
    parser.on('error', function(err){
        console.log(err);
    });
    
    parser.on('finish', function(){
        if (foundRanges.length > 0) {
            console.log('Found ' + foundRanges.length + ' matching ranges in ' + file);
            console.log(JSON.stringify(foundRanges, null, 2));
        }
        foundRanges = [];
        parseNextSource();
    });

    return parser;
};