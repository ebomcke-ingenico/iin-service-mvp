var fs = require('fs');
var parse = require('csv-parse');
var pad = require('utils-pad-string');
var BigNumber = require('big-number');
require('console-stamp')(console, '[HH:MM:ss.l]');
const uuidV4 = require('uuid/v4');
var jsonfile = require('jsonfile');
var minimist = require('minimist');

var cliArgs = minimist(process.argv.slice(2));

var config = {
    useFakeData: false,
    debug: false,
    debugCsvParse: false,
    debugPerformances: false,
    debugRange: false,
    debugConsolidation: false,
    countries: [],
    triggerWriteJsonScript: true,
    restrictToFiles: [
        'Accor business account',
        'Airplus',
        'Alsolia',
        'Amex',
        'Argencard',
        'ATU',
        'Aura',
        'Aurore',
        'Bancontact',
        'CABAL',
        'CB',
        'Cofinoga',
        'Consumax',
        'Credimas',
        'Dankort',
        'Diners',
        'Discover',
        'Elo',
        'Fnac',
        'Hiper.csv',
        'Hipercard',
        'Italcred',
        'JCB',
        'Kadicard',
        'Lider',
        'Mas.csv',
        'Mastercard',
        'MIR',
        'NARANJA',
        'Nativa',
        'NEVADA',
        'Nexo',
        'Printemps',
        'Privilege',
        'Pymenacion',
        'Tarjeta',
        'UATP',
        'UPI',
        'Visa',
    ],
    output: cliArgs.output || 'bindata.unformatted.json',
    prettyOutput: true
};

var logProcessingTime = function(start, msg) {
    msg = msg || '';
    if (config.debugPerformances) {
        console.log(msg + ' Processing time: ' + (Date.now() - start) + 'ms.');
    }
};

var logRange = function(msg, range, debug) {
    if (debug) console.log(msg + range.toString());
};

var extractNumericSubRange = function(range, subRangeLength) {
    return parseInt(range.substring(0, subRangeLength));
};

var isInRange = function(subRange, range) {
    var subRangeStart = extractNumericSubRange(range.rangeStart, subRange.toString().length);
    var subRangeEnd = extractNumericSubRange(range.rangeEnd, subRange.toString().length);
    return subRange >= subRangeStart && subRange <= subRangeEnd;
};

var processSingleRange = function(country, range) {
    var debug = config.debug;
    if (config.debugRange && isInRange(config.debugRange, range)) {
        debug = true;
    }
    if (debug) {
        console.log("");
        console.log('Now on range:       ' + range.toString());
    }
    if (consolidatedBinRangesPerCountry[country].getOverallUpperBound() < range.rangeStart) {
        if (debug) console.log('Range starts after overall upper bound (' + range.rangeStart + ' / ' + consolidatedBinRangesPerCountry[country].getOverallUpperBound());
        createNewRange(country, range);
    }
    else {
        var exactRange = consolidatedBinRangesPerCountry[country].getExactRange(range);
        if (exactRange) {
            if (debug) console.log('Found exact range ' + exactRange.toString());
            exactRange.addAcceptanceNetwork(range.acceptanceNetwork);
            exactRange.country = resolveCountry(exactRange, range);
            exactRange.currency = resolveCurrency(exactRange, range);
            consolidatedBinRangesPerCountry[country].addRanges([exactRange]);
        }
        else {
            var parentRange = consolidatedBinRangesPerCountry[country].getParentRange(range);
            if (parentRange) {
                if (debug) console.log('Found parent range  ' + parentRange.toString());
                createChildRange(country, parentRange, range);
            }
            else {
                var overlappingRanges = consolidatedBinRangesPerCountry[country].getOverlappingRanges(range);
                if (overlappingRanges) {
                    if (debug) console.log('Found ' + overlappingRanges.length + ' overlapping ranges');
                    createIntersectionRanges(country, overlappingRanges, range);
                }
                else {
                    createNewRange(country, range);
                }
            }
        }
    }
};

var createChildRange = function(country, parentRange, childRange) {
    var debug = config.debug;
    if (config.debugRange && isInRange(config.debugRange, parentRange)) {
        debug = true;
    }
    var parentStartTime = Date.now();
    var newRanges = [];
    if (BigNumber(parentRange.rangeStart).lt(BigNumber(childRange.rangeStart))) {
        var preRange = new ConsolidatedBinRange(parentRange.rangeStart, BigNumber(childRange.rangeStart).minus(1), parentRange.country, parentRange.currency, parentRange.acceptanceNetworks);
        newRanges.push(preRange);
        if (debug) console.log('Creating pre-range ' + preRange.toString());
    }

    var midRange = new ConsolidatedBinRange(childRange.rangeStart, childRange.rangeEnd, resolveCountry(parentRange, childRange), resolveCurrency(parentRange, childRange), parentRange.acceptanceNetworks);
    midRange.addAcceptanceNetwork(childRange.acceptanceNetwork);
    newRanges.push(midRange);
    if (debug) console.log('Creating mid-range ' + midRange.toString());

    if (BigNumber(parentRange.rangeEnd).gt(BigNumber(childRange.rangeEnd))) {
        var postRange = new ConsolidatedBinRange(BigNumber(childRange.rangeEnd).plus(1), parentRange.rangeEnd, parentRange.country, parentRange.currency, parentRange.acceptanceNetworks);
        newRanges.push(postRange);
        if (debug) console.log('Creating post-range ' + postRange.toString());
    }
    consolidatedBinRangesPerCountry[country].addRanges(newRanges);
    logProcessingTime(parentStartTime, '(createChildRange)');
};

var resolveCountry = function(refRange, nRange) {
    var debug = config.debug;
    if (config.debugRange && isInRange(config.debugRange, nRange)) {
        debug = true;
    }
    var country = refRange.country == 'WW' ? nRange.country :
        (refRange.country != nRange.country ? 'WW' : refRange.country);
    if (debug) console.log('Resolved country: ' + country + '(' + refRange.country + '/' + nRange.country + ')');
    return country;
}

var resolveCurrency = function(refRange, nRange) {
    var debug = config.debug;
    if (config.debugRange && isInRange(config.debugRange, nRange)) {
        debug = true;
    }
    var currency = refRange.currency == 'WWW' ? nRange.currency :
        (refRange.currency != refRange.currency ? 'WWW' : refRange.currency);
    if (debug) console.log('Resolved currency: ' + currency + '(' + refRange.currency + '/' + nRange.currency + ')');
    return currency;
}


var createIntersectionRanges = function(country, overlappingRanges, newRange, resolve) {
    var debug = config.debug;
    if (config.debugRange && isInRange(config.debugRange, newRange)) {
        debug = true;
    }
    var overlappingStartTime = Date.now();
    var newRanges = [];
    var lastRangeEnd = false;
    overlappingRanges.forEach(function(overlappingRange) {
        if (debug) console.log('Now processing range ' + overlappingRange.toString());
        if (BigNumber(overlappingRange.rangeStart).lte(BigNumber(newRange.rangeStart))) {
            if (BigNumber(overlappingRange.rangeStart).lt(BigNumber(newRange.rangeStart))) {
                var preRange = new ConsolidatedBinRange(overlappingRange.rangeStart, BigNumber(newRange.rangeStart).minus(1), overlappingRange.country, overlappingRange.currency, overlappingRange.acceptanceNetworks);
                newRanges.push(preRange);
                logRange('1 - new range ', preRange, debug);
            }
            var postRange = new ConsolidatedBinRange(newRange.rangeStart, overlappingRange.rangeEnd, resolveCountry(overlappingRange, newRange), resolveCurrency(overlappingRange, newRange), overlappingRange.acceptanceNetworks);
            postRange.addAcceptanceNetwork(newRange.acceptanceNetwork);
            newRanges.push(postRange);
            logRange('2 - new range ', postRange, debug);
        }
        else if (BigNumber(overlappingRange.rangeEnd).lte(BigNumber(newRange.rangeEnd))) {
            var startRange = lastRangeEnd ? lastRangeEnd.plus(1) : overlappingRange.rangeStart;
            var intermediateRange = new ConsolidatedBinRange(startRange, overlappingRange.rangeEnd, resolveCountry(overlappingRange, newRange), resolveCurrency(overlappingRange, newRange), overlappingRange.acceptanceNetworks);
            intermediateRange.addAcceptanceNetwork(newRange.acceptanceNetwork);
            newRanges.push(intermediateRange);
            logRange('3 - new range ', intermediateRange, debug);
        }
        else {
            var preRange = new ConsolidatedBinRange(overlappingRange.rangeStart, newRange.rangeEnd, resolveCountry(overlappingRange, newRange), resolveCurrency(overlappingRange, newRange), overlappingRange.acceptanceNetworks);
            preRange.addAcceptanceNetwork(newRange.acceptanceNetwork);
            newRanges.push(preRange);
            logRange('4 - new range ', preRange, debug);

            var postRange = new ConsolidatedBinRange(BigNumber(newRange.rangeEnd).plus(1), overlappingRange.rangeEnd, overlappingRange.country, overlappingRange.currency, overlappingRange.acceptanceNetworks);
            newRanges.push(postRange);
            logRange('5 - new range ', postRange, debug);
        }
        lastRangeEnd = BigNumber(overlappingRange.rangeEnd);
    });
    if (lastRangeEnd && lastRangeEnd.lt(BigNumber(newRange.rangeEnd))) {
        var postRange = new ConsolidatedBinRange(lastRangeEnd.plus(1), newRange.rangeEnd, newRange.country, newRange.currency, [newRange.acceptanceNetwork]);
        newRanges.push(postRange);
        logRange('6 - new range ', postRange, debug);
    }
    consolidatedBinRangesPerCountry[country].addRanges(newRanges);
    logProcessingTime(overlappingStartTime, '(createIntersectionRanges)');
};

var createNewRange = function(country, range) {
    var debug = config.debug;
    if (config.debugRange && isInRange(config.debugRange, range)) {
        debug = true;
    }
    var newRange = new ConsolidatedBinRange(range.rangeStart, range.rangeEnd, range.country, range.currency, [range.acceptanceNetwork]);
    if (debug) console.log('Creating new range: ' + newRange.toString());
    consolidatedBinRangesPerCountry[country].addRanges([newRange]);
};

var generateConsolidatedBinRanges = function() {
    rawBinRanges.sort();
    // if (cliArgs.fullranges) {
    //     rawBinRanges.consolidateConsecutiveRanges();
    // }
    if (cliArgs.groupbycountry) {
        rawBinRanges.countries.forEach(function(country) {
            if (!(config.countries.length > 0 && config.countries.indexOf(country) == -1)) {
                var allRanges = rawBinRanges.binranges[country].length;
                process.stdout.write('Started processing ' + country + ' (' + allRanges + ' ranges to process) ... ');
                var rangesCounter = 0;
                var countryStartTime = Date.now();
                consolidatedBinRangesPerCountry[country] = new ConsolidatedBinRanges();
                // var rangeStepForPercentages = false;
                // var currentPercentage = 0;
                // if (allRanges > 1000) {
                //     rangeStepForPercentages = Math.round(allRanges / 10);
                // }
                rawBinRanges.binranges[country].forEach(function(range) {
                    // if (rangeStepForPercentages && 
                    //     (rangesCounter % rangeStepForPercentages == 0) && rangesCounter > 0) {
                    //     currentPercentage += 10;
                    //     console.log('Processing status: ' + currentPercentage + '% (' + rangesCounter + ' out of ' + allRanges + ').');
                    // }
                    rangesCounter++;
                    return processSingleRange(country, range);
                });
                process.stdout.write('done.\n');
                logProcessingTime(countryStartTime);
            }
            consolidatedBinRangesPerCountry[country].sort();
            if (cliArgs.fullranges) consolidatedBinRangesPerCountry[country].consolidateConsecutiveRanges();
        });
    }
    else {
        consolidatedBinRangesPerCountry['ALL'] = new ConsolidatedBinRanges();
        rawBinRanges.countries.forEach(function(country) {
            if (!(config.countries.length > 0 && config.countries.indexOf(country) == -1)) {
                var allRanges = rawBinRanges.binranges[country].length;
                process.stdout.write('Started processing ' + country + ' (' + allRanges + ' ranges to process) ...');
                var rangesCounter = 0;
                var countryStartTime = Date.now();
                var rangeStepForPercentages = false;
                var currentPercentage = 0;
                if (allRanges > 1000) {
                    rangeStepForPercentages = Math.round(allRanges / 10);
                }
                rawBinRanges.binranges[country].forEach(function(range) {
                    if (rangeStepForPercentages &&
                        (rangesCounter % rangeStepForPercentages == 0) && rangesCounter > 0) {
                        currentPercentage += 10;
                        process.stdout.write(currentPercentage + '% done (' + rangesCounter + ') ...');
                    }
                    rangesCounter++;
                    return processSingleRange('ALL', range);
                });
                process.stdout.write('done (' + rangesCounter + ' ranges processed).\n');
                logProcessingTime(countryStartTime);
            }
        });
        consolidatedBinRangesPerCountry['ALL'].sort();
        if (cliArgs.fullranges) consolidatedBinRangesPerCountry['ALL'].consolidateConsecutiveRanges();
    }
    console.log('Finished generating consolidated binranges.');
    var json = formatBeforeWriting();
    if (config.triggerWriteJsonScript) {
        json = transformJson(json);
    }
    writeToJson(json, config.output, function() {
        addTestCases(json);
        writeToJson(json, config.output + '_withtest', function() {
            process.exit(0);
        });
    });
};

var formatBeforeWriting = function() {
    console.log('Sorting iin ranges per country and transforming to JSON object.');
    var jsonOutput = [];
    if (cliArgs.groupbycountry) {
        rawBinRanges.countries.forEach(function(country) {
            if (consolidatedBinRangesPerCountry[country]) {
                jsonOutput = jsonOutput.concat(consolidatedBinRangesPerCountry[country].binranges.map((r) => r.toJSON()));
            }
        });
    }
    else {
        jsonOutput = jsonOutput.concat(consolidatedBinRangesPerCountry['ALL'].binranges.map((r) => r.toJSON()));
    }
    return jsonOutput;
};

var writeToJson = function(json, outputName, cb) {
    console.log('Exporting results to json file.');
    var opts = config.prettyOutput ? { spaces: 2 } : {};
    jsonfile.writeFile(outputName, json, opts, function(err) {
        if (err) console.log(err);
        else {
            console.log('Results exported in json file (' + outputName + ').');
            cb();
        }
    });
};

var parseOneSourceFile = function() {
    if (sourceFiles.length > 0) {
        var sourceFile = sourceFiles.pop();
        if (config.restrictToFiles.length > 0 && !config.restrictToFiles.some((f) => sourceFile.indexOf(f) != -1)) {
            parseOneSourceFile();
        }
        else {
            var filename = 'source-files/' + sourceFile;
            if (config.useFakeData) {
                if (sourceFile.indexOf('fake-') !== -1) {
                    process.stdout.write('Reading file ' + sourceFile + '... ');
                    var input = fs.createReadStream(filename);
                    input.pipe(generateParser(function() {
                        process.stdout.write('done.\n');
                        parseOneSourceFile();
                    }));
                }
            }
            else {
                process.stdout.write('Reading file ' + sourceFile + '... ');
                var input = fs.createReadStream(filename);
                input.pipe(generateParser(function() {
                    process.stdout.write('done.\n');
                    parseOneSourceFile();
                }));
            }
        }
    }
    else {
        generateConsolidatedBinRanges();
    }
};

var generateParser = function(onFinish) {
    var parser = parse({
        columns: true
    });
    var rawBinRangesPerScheme = new RawBinRanges();

    parser.on('readable', function() {
        while (record = parser.read()) {
            if (!(config.countries.length > 0 && config.countries.indexOf(record.IssuerCountry) == -1)) {
                var range = rawBinRangesPerScheme.convertCsvItem(record);
                if (range) {
                    rawBinRangesPerScheme.addRange(range);
                }
            }
        }
    });

    parser.on('error', function(err) {
        console.log(err);
    });

    parser.on('finish', function() {
        rawBinRangesPerScheme.sort();
        if (cliArgs.fullranges) rawBinRangesPerScheme.consolidateConsecutiveRanges();
        rawBinRangesPerScheme.countries.forEach((country) => rawBinRangesPerScheme.binranges[country].forEach((range) => rawBinRanges.addRange(range)));
        onFinish();
    });

    return parser;
};

var RawBinRange = function(rangeStart, rangeEnd, country, currency, acceptanceNetwork) {
    this.rangeStart = pad(rangeStart, 19, {
        'rpad': '0'
    });
    rangeEnd = pad(rangeEnd, 19, {
        'rpad': '9'
    });
    // Quick hack to fix annoying as sh** Mastercard ranges that have range end like 559991999999999*0000*
    rangeEnd = rangeEnd.replace(/90000$/, '99999');
    // var end = (BigNumber(rangeEnd).mod(10) == 0) ? (BigNumber(rangeEnd).minus(1).toString()) : rangeEnd;
    // if (end.length < rangeEnd.length) {
    //     end = pad(end, rangeEnd.length, {
    //         'lpad': '0'
    //     });
    // }
    this.rangeEnd = rangeEnd;
    this.country = country;
    this.currency = currency;
    this.acceptanceNetwork = acceptanceNetwork;

    this.toString = function() {
        return this.rangeStart + ' to ' + this.rangeEnd + ' (country: ' + this.country + ', AN: ' + JSON.stringify(this.acceptanceNetwork) + ')';
    };
};

var RawBinRanges = function() {
    var self = this;

    self.binranges = {};
    self.countries = [];

    self.convertCsvItem = function(item) {
        var range = new RawBinRange(
            item.RangeStart,
            item.RangeEnd,
            item.IssuerCountry.trim() || 'WW',
            item.Currency.trim() || 'WWW', {
                scheme: item.Scheme,
                name: item.AcceptanceNetworkName || item.Scheme,
                chargeType: item.ChargeType || 'Credit',
                ranking: getRanking(item.AcceptanceNetworkName)
            }
        );
        if (config.debugCsvParse) {
            if (!config.debugRange || isInRange(config.debugRange, range)) {
                console.log('CSV item: ' + JSON.stringify(item, null, 2));
            }
        }
        if (range.rangeStart < range.rangeEnd) {
            return range;
        }
        else {
            return false;
        }
    };

    self.copyRange = function(range, newStart, newEnd) {
        var rangeStart = pad('' + newStart, 19, {
            'rpad': '0'
        });
        var rangeEnd = pad('' + newEnd, 19, {
            'rpad': '9'
        });
        return {
            rangeStart: rangeStart,
            rangeEnd: rangeEnd,
            country: range.country,
            currency: range.currency,
            acceptanceNetwork: range.acceptanceNetwork
        };
    };

    self.addRange = function(range) {
        var rCountry = cliArgs.groupbycountry ? range.country : 'ALL';
        if (self.countries.indexOf(rCountry) == -1) {
            self.countries.push(rCountry);
            self.binranges[rCountry] = [];
        }
        // Temporary hack to prevent Connect overlaps. Break all ranges apart if they overlap six digits boundaries, eg. 111111XXXXXXXXXXXXX - 111119XXXXXXXXXXXXX.
        var shortStart = extractNumericSubRange(range.rangeStart, 6);
        var shortEnd = extractNumericSubRange(range.rangeEnd, 6);
        if (!cliArgs.fullranges && (shortEnd - shortStart > 0)) {
            var newRanges = [];
            newRanges.push(self.copyRange(range, range.rangeStart, shortStart));
            for (var i = shortStart + 1; i < shortEnd; i++) {
                newRanges.push(self.copyRange(range, i, i));
            }
            newRanges.push(self.copyRange(range, shortEnd, range.rangeEnd));
            newRanges.forEach((r) => self.binranges[rCountry].push(r));
            if (config.debugCsvParse) {
                if (!config.debugRange || isInRange(config.debugRange, range)) {
                    console.log('Ranges: ' + JSON.stringify(newRanges, null, 2));
                }
            }
        }
        else {
            self.binranges[rCountry].push(range);
            if (config.debugCsvParse) {
                if (!config.debugRange || isInRange(config.debugRange, range)) {
                    console.log('Range: ' + JSON.stringify(range, null, 2));
                }
            }
        }
    };

    self.sort = function() {
        self.countries.forEach(function(country) {
            self.binranges[country].sort(function(r1, r2) {
                return r1.rangeStart - r2.rangeStart;
            });
        });
    };

    self.consolidateConsecutiveRanges = function() {
        self.countries.forEach(function(country) {
            process.stdout.write('Consolidating raw bin ranges for country ' + country + ': ' + self.binranges[country].length + '->');
            var combinedRanges = [];
            self.binranges[country].forEach(function(range, index) {
                if (index == 0) {
                    combinedRanges.push(range);
                }
                else {
                    var previousRange = combinedRanges[combinedRanges.length - 1];
                    if (config.debugConsolidation) {
                        console.log('Consolidation: comparing ranges:\n' + previousRange + "\n" + range);
                        // console.log('country test: ' + (range.country == previousRange.country));
                        // console.log('currency test: ' + (range.currency == previousRange.currency));
                        // console.log('acceptanceNetwork.scheme test: ' + (range.acceptanceNetwork.scheme == previousRange.acceptanceNetwork.scheme));
                        // console.log('acceptanceNetwork.chargeType test: ' + (range.acceptanceNetwork.chargeType == previousRange.acceptanceNetwork.chargeType));
                        // console.log('range boundary test: ' + BigNumber(previousRange.rangeEnd).plus(1).equals(BigNumber(range.rangeStart)));
                    }
                    if (range.country == previousRange.country &&
                        range.currency == previousRange.currency &&
                        range.acceptanceNetwork.scheme == previousRange.acceptanceNetwork.scheme &&
                        range.acceptanceNetwork.name == previousRange.acceptanceNetwork.name &&
                        range.acceptanceNetwork.chargeType == previousRange.acceptanceNetwork.chargeType) {
                        // Duplicate ranges
                        if (range.rangeStart == previousRange.rangeStart && range.rangeEnd == previousRange.rangeEnd) {
                            // do nothing
                            if (config.debugConsolidation) console.log('Duplicate ranges.');
                        }
                        else if (BigNumber(previousRange.rangeEnd).plus(1).equals(BigNumber(range.rangeStart))) {
                            // Consecutive ranges
                            if (config.debugConsolidation) console.log('Aggregating consecutive ranges.');
                            previousRange.rangeEnd = range.rangeEnd;
                        }
                        else if (BigNumber(previousRange.rangeStart).lte(BigNumber(range.rangeStart)) &&
                            BigNumber(previousRange.rangeEnd).gte(BigNumber(range.rangeEnd))) {
                            // Enclosed range - do nothing
                            if (config.debugConsolidation) console.log('Enclosed ranges.');
                        }
                        else {
                            combinedRanges.push(range);
                        }
                    }
                    else {
                        combinedRanges.push(range);
                    }
                }
            });
            self.binranges[country] = combinedRanges;
            process.stdout.write(self.binranges[country].length + '\n');
        });
    };
};

var getRanking = function(name) {
    switch (name) {
        case 'Bancontact':
            return 170;
        case 'Maestro':
            return 180;
        case 'CIRRUS':
            return 9999;
        case 'Visa Debit':
            return 190;
        case 'Visa Electron':
            return 195;
        case 'Mastercard':
            return 311;
        case 'Mastercard Debit':
            return 290;
        case 'Carte Bancaire':
            return 10;
        case 'CABAL':
            return 20;
        case 'NARANJA':
            return 30;
        case 'NEVADA':
            return 40;
        case 'ITALCRED':
            return 50;
        case 'ARGENCARD':
            return 60;
        case 'CONSUMAX':
            return 70;
        case 'MAS':
            return 80;
        case 'PYME NACION':
            return 90;
        case 'NATIVA':
            return 100;
        case 'AURA':
            return 110;
        case 'ELO':
            return 120;
        case 'HIPERCARD':
            return 130;
        case 'American express':
            return 200;
        case 'UPI':
            return 140;
        case 'Diners':
            return 145;
        case 'Discover':
            return 150;
        case 'JCB':
            return 160;
        case 'Visa':
            return 310;
        case 'Club Med':
            return 165;
        case 'American express':
            return 200;
        case 'Mastercard':
            return 300;
        case 'Switch':
            return 300;
        case 'American express':
            return 200;
        case 'Aurore':
            return 99;
        case 'Visa':
            return 310;
        case 'Dankort':
            return 210;
        case 'PassFR':
            return 24;
        case 'Mastercard':
            return 300;
        case 'PassIT':
            return 21;
        case 'PassES':
            return 22;
        case 'Naranja MO':
            return 31;
        case 'TARJETA SHOPPING':
            return 41;
        case 'Credencial':
            return 51;
        case 'LIDER':
            return 61;
        case 'KADICARD':
            return 71;
        case 'Credimas':
            return 81;
        case 'NEXO':
            return 91;
        case 'Coopeplus':
            return 101;
        case 'Favacard':
            return 111;
        case 'Club La Nacion':
            return 121;
        case 'Club Personal':
            return 131;
        case 'Club Speedy':
            return 141;
        case 'Club La Voz':
            return 151;
        case 'Clarin 366':
            return 161;
        case 'Club Arnet':
            return 171;
        case 'UPI':
            return 140;
        case 'UATP':
            return 15;
        case 'Airplus':
            return 16;
        case 'Cofinoga':
            return 11;
        case 'Fnac':
            return 12;
        case 'A.T.U.':
            return 13;
        case 'Accor Business Account':
            return 14;
        case 'Printemps':
            return 17;
        case 'Privilége':
            return 18;
        case 'Alsolia':
            return 19;
        default:
            return 99;
    }
};

var ConsolidatedBinRange = function(rangeStart, rangeEnd, issuerCountry, currency, acceptanceNetworks) {
    var self = this;

    self.id = uuidV4();
    self.rangeStart = '' + rangeStart;
    self.rangeEnd = '' + rangeEnd;
    self.country = issuerCountry;
    self.currency = currency;
    self.acceptanceNetworks = acceptanceNetworks.slice();

    self.addAcceptanceNetwork = function(acceptanceNetwork) {
        if (!self.acceptanceNetworks.some(function(an) {
                return an.scheme == acceptanceNetwork.scheme && an.name == acceptanceNetwork.name;
            })) {
            self.acceptanceNetworks.push(acceptanceNetwork);
        }
    };

    self.toString = function() {
        return self.rangeStart + ' to ' + self.rangeEnd + ' (country: ' + self.country + ', AN: ' + JSON.stringify(self.acceptanceNetworks) + ')';
    };

    self.toJSON = function() {
        return {
            rangeStart: self.rangeStart,
            rangeEnd: self.rangeEnd,
            issuerCountry: self.country,
            currency: self.currency,
            acceptanceNetworks: self.acceptanceNetworks
        };
    };
};

var ConsolidatedBinRanges = function() {
    var self = this;
    self.binranges = [];

    self.lastInsertIndex = 0;

    self.addRanges = function(ranges) {
        self.binranges.splice.apply(self.binranges, [self.lastInsertIndex + 1, 0].concat(ranges));
        self.lastInsertIndex = self.binranges.length - 1;
    };

    self.getOverallUpperBound = function() {
        if (self.binranges.length > 0) {
            return self.binranges[self.binranges.length - 1].rangeEnd;
        }
        else return 0;
    };

    self.getExactRange = function(range) {
        var matchIndex = false;
        self.binranges.some(function(r, index) {
            var match = (range.rangeStart == r.rangeStart && range.rangeEnd == r.rangeEnd);
            if (match) {
                matchIndex = index;
                return true;
            }
            else {
                return false;
            }
        });
        if (matchIndex) {
            self.lastInsertIndex = matchIndex;
            return self.binranges.splice(matchIndex, 1)[0];
        }
        else {
            return false;
        }
    };

    self.getParentRange = function(range) {
        var matchIndex = false;
        self.binranges.some(function(r, index) {
            var match = (range.rangeStart >= r.rangeStart && range.rangeEnd <= r.rangeEnd);
            if (match) {
                matchIndex = index;
                return true;
            }
            else {
                return false;
            }
        });
        if (matchIndex !== false) {
            self.lastInsertIndex = matchIndex;
            return self.binranges.splice(matchIndex, 1)[0];
        }
        else {
            return false;
        }
    };

    self.hasOverlappingRanges = function(range) {
        var matchIndex = false;
        return self.binranges.some(
            (r) => ((range.rangeStart <= r.rangeStart && range.rangeEnd > r.rangeStart && range.rangeEnd < r.rangeEnd) ||
                (range.rangeStart < r.rangeStart && range.rangeEnd > r.rangeEnd) ||
                (range.rangeStart >= r.rangeStart && range.rangeStart < r.rangeEnd && range.rangeEnd > r.rangeEnd))
        );
    };

    self.getOverlappingRanges = function(range) {
        var matchIndex = false;
        var matchLength = 0;
        self.binranges.forEach(function(r, index) {
            var match = ((range.rangeStart <= r.rangeStart && range.rangeEnd > r.rangeStart && range.rangeEnd < r.rangeEnd) ||
                (range.rangeStart < r.rangeStart && range.rangeEnd > r.rangeEnd) ||
                (range.rangeStart >= r.rangeStart && range.rangeStart < r.rangeEnd && range.rangeEnd > r.rangeEnd));
            if (match) {
                if (!matchIndex) {
                    matchIndex = index;
                }
                matchLength++;
            }
        });
        if (matchIndex) {
            self.lastInsertIndex = matchIndex;
            return self.binranges.splice(matchIndex, matchLength);
        }
        else {
            return false;
        }
    };

    self.sort = function() {
        self.binranges.sort(function(r1, r2) {
            if (r1.rangeStart == r2.rangeStart) {
                return r1.rangeEnd - r2.rangeEnd;
            }
            else {
                return r1.rangeStart - r2.rangeStart;
            }
        });
    };

    self.consolidateConsecutiveRanges = function() {
        process.stdout.write('Consolidating bin ranges for country : ' + self.binranges.length + '->');
        var combinedRanges = [];
        self.binranges.forEach(function(range, index) {
            if (index == 0) {
                combinedRanges.push(range);
            }
            else {
                var previousRange = combinedRanges[combinedRanges.length - 1];
                if (config.debugConsolidation) {
                    console.log('Consolidation: comparing ranges:\n' + previousRange + "\n" + range);
                }
                if (range.country == previousRange.country &&
                    range.currency == previousRange.currency &&
                    BigNumber(previousRange.rangeEnd).plus(1).equals(BigNumber(range.rangeStart)) &&
                    sameAcceptanceNetworks(range.acceptanceNetworks, previousRange.acceptanceNetworks)) {
                    if (config.debugConsolidation) console.log('Aggregating ranges!');
                    previousRange.rangeEnd = range.rangeEnd;
                }
                else {
                    combinedRanges.push(range);
                }
            }
        });
        self.binranges = combinedRanges;
        process.stdout.write(self.binranges.length + '\n');
    };

    self.toJSON = function() {
        return self.binranges.map((r) => r.toJSON());
    };
};

var sameAcceptanceNetworks = function(ans, cans) {
    return ans.every((an) => {
        return cans.some((can) => sameAcceptanceNetwork(an, can));
    }) && cans.every((can) => {
        return ans.some((an) => sameAcceptanceNetwork(an, can));
    });
}
var sameAcceptanceNetwork = function(an, can) {
    return an.scheme == can.scheme &&
        an.name == can.name &&
        an.chargeType == can.chargeType;
}

var consolidatedBinRangesPerCountry = {};
var rawBinRanges = new RawBinRanges();
var sourceFiles = [];

console.log('Starting generation of consolidated bin ranges.');
fs.readdir('source-files', function(err, items) {
    if (err) {
        console.log('Error reading source files.');
        console.log(err);
        process.exit(-1);
    }
    sourceFiles = items;
    parseOneSourceFile();
});

var transformToOldFormat = function(r) {
    return {
        RangeStart: r.rangeStart,
        RangeEnd: r.rangeEnd,
        Country: {
            CountryOfIssueCode: r.issuerCountry
        },
        Currency: {
            DomicileCurrencyCode: r.currency
        },
        AcceptanceNetwork: r.acceptanceNetworks.map((an) => {
            return {
                Scheme: an.scheme,
                Ranking: an.ranking,
                ChargeType: an.chargeType,
                Name: an.name
            };
        })
    };
};

var copyNameToScheme = function(formattedRange) {
    formattedRange.acceptanceNetworks.forEach(function(an) {
        // 1. Copy Acceptance Network Name to Scheme as Connect maps on the Scheme
        an.scheme = an.name;
    });
};

var dirtyTempHack = function(formattedRange) {
    formattedRange.acceptanceNetworks.forEach(function(an) {
        // 2. Change all Visa Debit & Visa Electron to normal Visa Credit
        if (/visa/i.test(an.scheme)) {
            an.scheme = 'Visa';
            an.name = 'Visa';
            an.chargeType = 'Credit';
            changed.allVisa++;
        }
        // 3. Change all Mastercard Debit & Mastercard to scheme Mastercard & chargeType Debit
        if (/mastercard/i.test(an.scheme)) {
            an.scheme = 'Mastercard';
            an.name = 'Mastercard';
            an.chargeType = 'Credit';
            changed.allMastercard++;
        }
        /* // 2. Change all Mastercard chargeType Debit to chargeType Credit to match Connect mapping. This is so f***ing stupid.
        // if (/^mastercard$/i.test(an.scheme) && /^debit$/i.test(an.chargeType)) {
        //     an.scheme = 'Mastercard';
        //     an.chargeType = 'Credit';
        //     changed.mastercardDebit++;
        // }
        
        // // 3. Change all Mastercard Debit chargeType Debit to chargeType Credit to match Connect mapping. This is so f***ing stupid.
        // if (/^mastercard debit$/i.test(an.scheme) && /^debit$/i.test(an.chargeType)) {
        //     an.scheme = 'Mastercard Debit';
        //     an.chargeType = 'Credit';
        //     changed.mastercardDebitDebit++;
        // }
        
        // // 4. Change all Visa chargeType Debit to chargeType Credit to match Connect mapping. This is so f***ing stupid.
        // if (/^visa$/i.test(an.scheme) && /^debit$/i.test(an.chargeType)) {
        //     an.scheme = 'VISA';
        //     an.chargeType = 'Credit';
        //     changed.visaDebit++;
        // }
        
        // // 5. Change all Visa Debit chargeType Credit to chargeType Debit to match Connect mapping. This is so f***ing stupid.
        // if (/^visa debit$/i.test(an.scheme) && /^credit$/i.test(an.chargeType)) {
        //     an.scheme = 'Visa Debit';
        //     an.chargeType = 'Debit';
        //     changed.visaDebitCredit++;
        // }
        
        // // 6. Change all Visa Electron chargeType Credit to chargeType Debit to match Connect mapping. This is so f***ing stupid.
        // if (/^visa electron$/i.test(an.scheme) && /^credit$/i.test(an.chargeType)) {
        //     an.scheme = 'Visa Electron';
        //     an.chargeType = 'Debit';
        //     changed.visaElectronCredit++;
        // }
        // console.log('Updated an: ' + JSON.stringify(an, null, 2));*/
    });
};

var changed = {
    mastercardDebit: 0,
    mastercardDebitDebit: 0,
    visaDebit: 0,
    visaDebitCredit: 0,
    visaElectronCredit: 0,
    allMastercard: 0,
    allVisa: 0
};


var transformJson = function(json) {
    var removeSubBrands = cliArgs.nosubbrands;
    var mapOnSchemes = cliArgs.maponschemes;
    var oldFormat = cliArgs.oldformat;

    var formatted = [];

    console.log('Transforming IIN file: output to ' + (!oldFormat ? 'new' : 'old') + ' format, mapping on ' + (mapOnSchemes ? 'schemes' : 'acceptance network names') + (removeSubBrands ? ', removing all sub brands' : '') + '.');
    json.forEach((r) => {
        if (r.issuerCountry == 'ALL') r.issuerCountry = 'WW';
        if (r.currency == 'ALL') r.currency = 'WWW';
        r.acceptanceNetworks.forEach(function(an) {
            an.chargeType = (an.chargeType == 'UNKNOWN' ? 'Credit' : an.chargeType);
            an.ranking = an.ranking || 99;
        });
        if (mapOnSchemes) {
            copyNameToScheme(r);
        }
        if (removeSubBrands) {
            dirtyTempHack(r);
        }
        if (oldFormat) {
            r = transformToOldFormat(r)
        }
        formatted.push(r);
    });

    if (mapOnSchemes) {
        console.log("Ranges changed from Mastercard - Debit to Mastercard - Credit: " + changed.mastercardDebit);
        console.log("Ranges changed from Mastercard Debit - Debit to Mastercard Debit - Credit: " + changed.mastercardDebitDebit);
        console.log("Ranges changed from Visa - Debit to Visa - Credit: " + changed.visaDebit);
        console.log("Ranges changed from Visa Debit - Credit to Visa Debit - Debit: " + changed.visaDebitCredit);
        console.log("Ranges changed from Visa Electron - Credit to Visa Electron - Debit: " + changed.visaElectronCredit);
        console.log("Ranges changed to Mastercard Credit: " + changed.allMastercard);
        console.log("Ranges changed to Visa Credit: " + changed.allVisa);
    }

    return formatted;
};

var addTestCases = function(json) {
    var oldFormat = cliArgs.oldformat;
    var mapOnSchemes = cliArgs.maponschemes;

    console.log('Adding sandbox testcases.');
    var uniqueTestCaseRanges = [];
    var alltcRanges = require('./testcase-ranges.json');
    alltcRanges.forEach(function(tcr) {
        if (!uniqueTestCaseRanges.some(function(ur) {
                return ur.RangeStart == tcr.RangeStart &&
                    ur.RangeEnd == tcr.RangeEnd &&
                    ur.Country.CountryOfIssueCode == tcr.Country.CountryOfIssueCode &&
                    ur.Currency.DomicileCurrencyCode == tcr.Currency.DomicileCurrencyCode;
            })) {
            uniqueTestCaseRanges.push(tcr);
        }
    });
    console.log('Adding ' + uniqueTestCaseRanges.length + ' unique test case ranges from ' + alltcRanges.length + ' test case ranges.');
    uniqueTestCaseRanges.forEach(function(tcr) {
        if (mapOnSchemes) {
            tcr = dirtyTempHack(tcr);
        }
        if (!oldFormat) {
            tcr = {
                rangeStart: tcr.RangeStart,
                rangeEnd: tcr.RangeEnd,
                issuerCountry: tcr.Country.CountryOfIssueCode,
                currency: tcr.Currency.DomicileCurrencyCode,
                acceptanceNetworks: tcr.AcceptanceNetwork.map((an) => {
                    return {
                        scheme: an.Scheme,
                        ranking: an.Ranking,
                        chargeType: an.ChargeType,
                        name: an.Name
                    };
                })
            };
        }
        json.push(tcr);
    });
}
