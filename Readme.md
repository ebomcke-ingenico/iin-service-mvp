IIN service MVP
===============

What is this?
-------------

This is a basic implementation of the IIN service in Node.js. It follows the specs that have been given to the squad that implements the "real" service
(see https://confluence.techno.ingenico.com/display/UxT/UXT-19+-+MVP).

Functionalities are very limited, it only allows a user to generated a JSON file that is compatible with the API gateway of Connect.

How to install it?
------------------

Clone the git repository on any environment that has Node.js installed (tested on my personal MacBook & on cloud9).

Install all dependencies with ``npm install``.

What does it actually do?
-------------------------

Generating the JSON file is done in a couple of steps :

* Read all CSV source files that are in the ``source-files/`` directory.
* Consolidate all BIN ranges per issuing country.
* Write all consolidated BIN ranges to JSON file.
* Change format of JSON file to the current Connect format.
* Insert test cases.
* Convert line ending formats from unix to ms-dos.
* Archive the resulting JSON file in a zip file.

How do I use it?
----------------

Generate new set of iin files: ``./generate-iin-file.sh``. This will create two zip files, one without test cases for production (%Y-%m-%dT%H%M%S.1234567+0100.zip), and the other with
test cases for INT/TEST/SANDBOX (%Y-%m-%dT000000.0000000+0100.zip).
HIVE_NAME}} includetestcases``.

Anything else?
--------------

There is another tool that can help debug issues with ranges. It looks through the entire JSON file to identify ranges associated to a BIN,
and finds the matching ranges in the source csv files as well. Note that it requires the JSON file to be there in clear (non archived).

Usage : ``node find-bin.js {{JSON_FILE_NAME}} {{BIN}}``.

There is also a mock API that reproduces the behaviour of the Connect get IIN details call to perform some quick validation on the content of the IIN file.

Usage : ``node api.js --input {{JSON_FILE_NAME}}``.