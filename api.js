var restify = require('restify');
var fs = require('fs');
var pad = require( 'utils-pad-string' );
var minimist = require('minimist');
var cliArgs = minimist(process.argv.slice(2));
var inputJson = cliArgs.input;

if (!inputJson) {
    console.log('Please specify the input json using the --input cli param.');
    process.exit(-1);
}

console.log('Reading bin data from ' + inputJson + '.');
var bindata = JSON.parse(fs.readFileSync(inputJson, 'utf8'));
console.log('Bin data loaded, ' + bindata.length + ' ranges available.');

function respond(req, res, next) {
    var bin = req.body.bin;
    if (!bin) {
        res.send(400, JSON.stringify({
            errorId: 'IIN-ERROR-0',
            errors: [{
                id: 'PARAMETER_NOT_FOUND_IN_REQUEST',
                message: 'No bin found in request body.'
            }]
        }));
        next();
    } else {
        var binsearch = pad(bin, 19, {
            'rpad': '0'
        });
        var range = bindata.filter(function(range) {
            return binsearch >= range.rangeStart && binsearch <= range.rangeEnd;
        });
        if (range.length == 0) {
            res.send(404, JSON.stringify({
                errorId: 'IIN-ERROR-2',
                errors: [{
                    id: 'IIN_NOT_FOUND',
                    message: 'No bin range found for given bin.'
                }]
            }));
        } else if (range.length == 1) {
            range = range[0];
            console.log(range);
            var iinResponse = {
                countryCode: range.issuerCountry,
                isAllowedInContext: true
            };
            if (range.acceptanceNetworks.length == 1) {
                iinResponse.paymentProductId = getPaymentProductId(range.acceptanceNetworks[0].scheme);
            } else {
                iinResponse.coBrands = range.acceptanceNetworks.map(function(an) {
                    return {
                        paymentProductId: getPaymentProductId(an.scheme),
                        isAllowedInContext: true
                    };
                });
            }
            res.send(200, iinResponse);
        } else {
            res.send(500, JSON.stringify({
                errorId: 'IIN-ERROR-1',
                errors: [{
                    id: 'MULTIPLE_RANGES_FOUND',
                    message: 'Multiple ranges found for bin ' + req.body.bin + ' (' + range.length + ' ranges).'
                }]
            }));
        }
        next();
    }
}

var getPaymentProductId = function(scheme) {
    var mapping = {
        'Visa': 1,
        'American Express': 2,
        'MasterCard': 3,
        'Visa Debit': 114,
        'Maestro': 117,
        'MasterCard Debit': 119,
        'Visa Electron': 122,
        'JCB': 125,
        'Discover': 128,
        'Carte Bancaire': 130,
        'Diners Club': 132,
        'Cabal': 135,
        'Naranja': 136,
        'Nevada': 137,
        'Italcred': 139,
        'Argencard': 140,
        'Consumax': 141,
        'Mas': 142,
        'Pyme Nacion': 144,
        'Nativa': 145,
        'Aura': 146,
        'ELO': 147,
        'Hipercard': 148,
        'Tarjeta Shopping': 149,
        'Bancontact': 3012,
        'Dankort': 123,
        'Accor Business Account': 3002,
        'A.T.U': 3004,
        'AIRPLUS': 3001,
        'UATP': 3008
    };
    return mapping[scheme];
};

var server = restify.createServer();
server.use(restify.bodyParser());
server.post('/services/getIINdetails', respond);

server.get('/services/getIINdetails', function(req, res, next) {
    res.send(200, "There be dragons.");
});

server.get('/', function(req, res, next) {
    res.send(200, "There be dragons.");
});

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
  var addr = server.address();
  console.log("API server listening at", addr.address + ":" + addr.port);
});
