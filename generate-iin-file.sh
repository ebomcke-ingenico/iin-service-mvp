#!/bin/bash

node parse.js --output=bindata.latest.json  --fullranges=0;
./package-file-zip.sh bindata.latest.json `date '+%Y-%m-%dT%H%M%S.1234567+0100'`.zip;
./package-file-zip.sh bindata.latest.json_withtest `date '+%Y-%m-%dT000000.0000000+0100'`.zip;